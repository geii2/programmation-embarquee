---
layout: layouts/page.njk
title: Les chaînes de caractères
---

## L'objet string

Les chaînes de caractères sont prises en charge par la classe `String`. le type associé est `string`.

Les chaîne de caractères littérales sont entourées par des guillemets ".

```csharp
string texte = "Bonjour tout le monde";
```

### Caractères spéciaux

Pour spécifier des caractères spéciaux on utilise le caractère `\` suivi d'un code.

`\n` pour un retour à la ligne, `\t` pour une tabulation, `\"` pour un guillemet à l'intérieur de la chaîne, puisque'il sert en temps normal à la délimitation, `\\` pour le caractère \ puisque normalement il sert d'échappement pour les caractères spéciaux.

### chaîne de caractères verbatim

Une chaîne de caractères verbatim est une chaîne dont les caractères spéciaux et les retours à la ligne sont désactivés. Ce qui permet  d'écrire sur plusieurs ligne.


## Propriétés

Longueur d'une chaîne

```csharp
texte.Length
```

## Fonctions

### Majuscule

Transformer toutes les lettres en majuscule. 

> Attention une chaîne de caractères est un objet **immutable**, non modifiable. Il faut faire une nouvelle affection même si c'est dans la même variable.
{.warning}

```csharp
texte = texte.ToUpper();
```

### Minuscule

Transformer toutes les lettres en minuscule

```csharp
texte = texte.ToLower();
```

### Suppression des espaces

```csharp
texte = texte.Trim();
```

### Complète

```csharp
string texte = "42";
texte = texte.PadLeft(5, '0');
texte = "00042";
```

### Découpage

```csharp
string texte = "a,b,c";
string[] tableau = texte.Split(',');
```

### Joindre

```csharp
string[] tableau = ["a","b","c"];
string texte = tableau.Join(',');
```

## Conversion

Convertir un texte dans la valeur correspondante. 42 et "42" ont une représentation identique mais dans un cas nous avons un nombre avec lequel nous pouvons faire des calculs, l'autre est une chaîne composées de deux caractères le `4` et le `2`.

```csharp
int nombre = int.Parse("42");
```