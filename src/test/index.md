---
layout: layouts/page.njk
title: Tests
---


Un projet de test permet d'effectuer des tests de manière automatique.

```csharp
Assert.Equal<string>(valeurAttendue, ValeurActuelle);
```

```csharp
Assert.Equal<string>(valeurAttendue, ValeurActuelle);
```
:Assertion
  Une assertion est un énoncé présenté comme vrai mais qui n'est pas encore vérifié voire non vérifiable, et potentiellement faux. 