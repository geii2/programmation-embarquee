---
layout: layouts/page.njk
title: Types valeurs
---

Les types valeur sont des **types primitifs** ou types simples. 

Ils sont associés à des **classes englobantes** ce qui permet d'associer des fonctions de conversion à chaque type précis. Tout est objet même les types primitifs sont reliés à des objets.

## Entiers

type|taille|classe|plage
---|---|---|---
byte   | 8 bits        | Byte  | 0 à 255
sbyte  | 8 bits signé  | SByte |-128 à 127
short  | 16 bits signé | Int16 | -32 768 à 32 767
ushort | 16 bits       | UInt16 | 0 à 65 535
int    | 32 bits signé | Int32 | -2 147 483 648 à<br> 2 147 483 647
uint   | 32 bits       | UInt32 | 0 à 4 294 967 295
long   | 64 bits signé | Int64 | -9 223 372 036 854 775 808 à<br> 9 223 372 036 854 775 807
ulong  | 64 bits       | UInt64 | de 0 à 18 446 744 073 709 551 615 

Il  existe aussi des types de taille variable `nint` et `nuint` qui sont des entiers de 32 bits ou 64 bits suivant la plateforme sur laquelle le programme s'execute.	Les classes associées sont IntPtr et UIntPtr. Ces types sont utilisés lorsqu'un programme C# a besoin de communiquer des adresses de pointeurs à des programmes écrit en C.

### Exemple de fonctionnement d'une classe associée

**Propriétés :** 

`Int32.MinValue` et `Int32.MaxValue`

**Méthodes :**

 Méthode de conversion d'un texte (chaîne de caractère) représentant un nombre en type primitif entier : `Intr32.Parse`

```csharp
int nombre = Int32.Parse("42");
```

Méthode de conversion d'un entier en une représentation sous forme de texte.

```csharp
int nombre = 42;
string texte = nombre.ToString();
```

ToString accepte un paramètre pour spécifier la représentation souhaitée.
code|format|résultat|milliers
---|---|---|---
G  |général     |42         |   
C  |monétaire (currency)   |42,00 €    | -15 474,00 €
D5 |décimal     |00042      | 
E3 |exponentiel |4,200E+001 | 
e2 |exponentiel |4,20e+001  | 
F3 |virgule fixe|42,000     | -15474,000
N  |numérique   |42,00      | -15 474,00
P  |pourcentage |4 200,00 % | 
X  |hexadécimal |2A         | FFFFC38E

### Conversion

La conversion d'un entier d'un certaine taille vers un type plus large est automatique.

```csharp
int nombre = 42;
long total = nombre;
```

La conversion d'un entier d'un certaine taille vers un type moins large doit être spécifié avec un `cast`. Le cas échéant on accepte une perte de données si le nombre de départ est supérieur à la limite du type d'arrivée.

```csharp
long total = 42;
int nombre = (int)total;
```

## Réels

type|taille|classe|plage|précision
---|---|---|---|---
float |	32 bits | Single | ±1,5 x 10<sup>−45</sup> à ±3,4 x 1038 | 6-9 
double 	|64 bits| Double | ±5,0 × 10<sup>−324</sup> à ±1,7 × 10308 | 15-17 
decimal |128 bits| Decimal | ±1,0 x 10<sup>-28</sup> à ±7,9228 x 1028 |	28-29

Le type Decimal représente des nombres décimaux approprié pour les calculs financiers qui requièrent un grand nombre de chiffres significatifs et aucune erreur d’arrondi. Le type Decimal n’élimine pas le besoin d’arrondi. Au lieu de cela, il réduit les erreurs dues à l’arrondi.

### Précision

En informatique un nombre réel n'a pas de précision décimale mais un précision binaire. C'est à dire que les nombres représentés sont des puissances de 2 mais pas de 10.

Supposons un nombre réel représenté sur 8 bits. Le pas est égal à 1 / 256 soit 0,00390625. Dans ce système le nombre décimal 0,4 n'a pas de représentation exacte. Le nombre le plus proche est 102 * 1 / 256 soit 0,39843750 

Multiplicateur|Nombre
--:|---|
100|0,39062500
101|0,39453125
102|0,39843750
100|0,40234375

## Booléen

Le mot clé `bool` est un type pour les valeurs booléennes, qui peuvent prendre une des 2 valeurs `true` ou `false`.

## Caractère

Le mot clé `char` est un type de 16 bits représentant un caractère unicode. La définition d'un caractère utilise l'apostrophe simple.

```csharp
char c = 't';
```