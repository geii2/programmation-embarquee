---
layout: layouts/page.njk
title: Modificateurs d’accès
---

Il est possible de restreinte l'accès aux membres d'une classe.

**public** : Tout autre code du même programme ou d'un autre module qui y fait référence peut accéder au type ou au membre.

**private** : Seul du code de la même classe peut accéder au type ou au membre.

**protected** : Seul du code de la même classe, ou du code d’une classe dérivée de cette classe, peut accéder au type ou au membre.

**internal** : Tout code du même programme, mais pas d'un autre module, peut accéder au type ou au membre.
