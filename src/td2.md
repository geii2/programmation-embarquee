---
layout: layouts/page.njk
title: Travaux pratiques
---

Créer une classe pour la modélisation du client, ainsi que pour la modélisation des comptes.

Céer une nouvelle classe pour le gérer les interactions entre l'utilisateur et la classe `Client`. Appelez cette classe `ClientController`.

Faire de même pour les comptes.

Dans le programme principal écrire un fonction pour afficher le menu principal

```
┌────────────────────────────────────────┐
│ Automate Bancaire                      │
│ 1 Client                               │
│ 2 Compte                               │
│ 3 Quitter                              │
│                                        │
│                                        │
└────────────────────────────────────────┘
```

Dans chacun des controleurs (client et compte) ajouter une fonction menu pour afficher le menu relatif.

Menu Client
```
┌────────────────────────────────────────┐
│ Client                                 │
│ 1 Créer                                │
│ 2 Lister                               │
│ 3 Exporter                             │
│ 4 Importer                             │
│ 5 Retour au menu principal             │
└────────────────────────────────────────┘
```

Menu Compte
```
┌────────────────────────────────────────┐
│ Compte                                 │
│ 1 Créer                                │
│ 2 Lister                               │
│ 3 Exporter                             │
│ 4 Importer                             │
│ 5 Retour au menu principal             │
└────────────────────────────────────────┘
```

Ajouter une propriété de type chaine de caractère qui permet au programme de savoir dans quel controleur l'utilisateur se situe.

``` csharp
string controller;
```

Les valeurs de la variable controller peuvent être
```
"" : menu principal
"client" : menu client
"compte" : menu compte
```

Ajouter une fonction `GererController` Dans le programme principal. Lire une touche d'entrée au clavier et suivant le controleur actif envoyé le code de la touche à la fonction `Choix` du controleur en question ou bien au programme principal.

Dans chaque fonction `Choix` suivant le code touche reçu effectuer l'action corresopndante.
La fonction `Choix` retourne un état binaire (vrai/faux) pour indiquer à la fonction `GererController` si l'utilisateur reste dans le contrôleur dans lequel il y est ou bien si il le quitte.

Dans la fonction principale du programme faire un boucle infinie sur la gestion de controleurs tant que l'on a pas quitter le controleur principal.
