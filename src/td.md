---
layout: layouts/page.njk
title: Exercices
---

Créer un compte sur Lucidchart https://lucid.app/

## Distributeur de snack

Modéliser à l'aide d'un diagramme de classes une distributeur de snackLe distributeur possède plusieurs **compartiments**. chaque compartiment est approvisionné avec un certain nombre de **produit**.

Les produits ont des **prix** différents

```mermaid
classDiagram

    class Produit{
        +char[10] nom
        +float prix
    }
```

## Domotique

Modéliser à l'aide d'un diagramme de classes une installation domotique

Il faut pouvoir gérer le **système de chauffage**, la **production d'eau chaude**, les **volets roulants**, l'**état des fenêtres**, la **ventilation**, les différentes **pièces** avec les **mesures d'ambiance**, le **portail d'entrée**.

## Entreprise de logistique

Une entreprise de logistique stocke et livre des **produits** pour ses **clients**.

L'entreprise possède plusieurs **entrepôt**, chaque entrepot possède des **zones de stockage**. Les produits sont stockés dans les zones de stockage.L'entreprise possède plusieurs **camions** de différentes taille. Les camions sont utilisés pour **livrer** les produits soit entre les entrepôts soit entre un entrepôt et un client.

L'entreprise possède des **cuves de carburant** pour faire le plein de ses camions.

## Bowling

Un bowling est un endroit géré par un système automatisé qui prend en charge le déroulement des parties.

Un **bowling** est constitué de plusieurs pistes (lanes).

Une **piste** ou est caractérisée par son numéro.

Des **joueurs** identifiés par leur nom, jouent ensemble une partie.

Une **partie** est constituée de 10 **carreaux** (frames).

Lors d'un carreau, chaque joueur tentent chacun leur tour, de renverser les 10 quilles10 quilles. Chaque joueur effectue un ou 2 **lancés**.

Le nombre **points** correspond aux nombre de quilles tombées lors d'un carreau.Si les 10 quilles sont renversées au bout du deuxième lancé, on parle alors d'une **réserve** (spare). 

On ajoute au score les points du lancé suivant.Si les 10 quilles sont renversées dés le premier lancé, on parle alors d'**abat** (strike). On ajoute au score les points des 2 lancés suivants.Si un **abat** ou une **réserve** arrive au 10e carreau, on ajoute des lancés pour compter les points supplémentaire.

## Compte bancaire

Modéliser à l'aide d'un diagramme de classes un automate de gestion bancaire.

Des **clients** ouvrent des **comptes**, ils peuvent retirer ou déposer de l'argent.