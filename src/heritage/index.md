---
layout: layouts/page.njk
title: Héritage de classe
---

L’héritage est un des attributs fondamentaux de la programmation orientée objet. Il vous permet de définir une classe enfant qui réutilise (hérite), étend ou modifie le comportement d’une classe parente ou classe de base. La classe qui hérite des membres de la classe de base est appelée la classe dérivée.

```csharp
public class Vehicule
{
}

public class Voiture : Vehicule
{
}

public class SUV : Voiture
{
}
```

La classe Voiture reprend les membres de la classe Vehicule, et ajoute ses propres membres spécifiques. La classe SUV reprend tous les membres de la classe Voiture, même ceux hérités de la classe Vehicule, et ajoute ses propres membres spécifiques.

## Surcharge / overriding

Une classe dérivée ajoute des méthodes personnalisées mais peut aussi remplacer les méthodes de la classe de base pour un traitement plus spécifique.


```csharp
public class Vehicule
{
	public virtual void Tourner() { … }
}
```

```
public class Voiture : Vehicule
{
	public override void Tourner() { … }
}
```

Le mot clé `virtual` sert à modifier une méthode, une propriété, et leur permet d’être substitués dans une classe dérivée. L’implémentation d’un membre virtuel peut (ou pas) être modifiée par un membre de substitution dans une classe dérivée.

Par défaut, les méthodes ne sont pas virtuelles. Vous ne pouvez pas substituer une méthode non virtuelle. La méthode surchargée doit avoir strictement la même signature que la méthode de base. Il faut faire attention à l'éventuel surcharge/overloading d'une méthode.

Si la méthode dérivée ne fait que compléter le fonctionnement de la méthode de base, elle peut appeler la méthode base en utilisant le mot clé `base`.

```csharp
public class Vehicule
{
	public virtual void Tourner() { … }
}

public class Voiture : Vehicule
{
	public override void Tourner() {
		base.Tourner();
		… // Code spécifique en plus du code de base
	}
}
```
