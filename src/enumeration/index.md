---
layout: layouts/page.njk
title: Enumérations
---

> Un type **enum** est un type qui définit un ensemble de valeurs numériques distinctes  qui déclare un ensemble de constantes étiquettées.

```csharp
public enum CodeErreurs
{
  TempratureElevee,
  PompeArretee,
  ArrivéeEau,
  ArrivéeGaz
  DetectionFlamme
}
```

Chaque étiquette se voit atttribuer automatiquement une valeur numérique en incrémentant de 1.

```
TempratureElevee = 0
PompeArretee = 1
ArrivéeEau = 2
ArrivéeGaz = 3
DetectionFlamme = 4
```

Il est possible de fixer les valeurs des constantes, celles sont valeurs sont calculées en incrémentant la valeur précédente par 1.


```csharp
public enum CodeErreurs
{
  TempratureElevee = 10,
  PompeArretee,
  ArrivéeEau = 100,
  ArrivéeGaz
  DetectionFlamme
}
```
