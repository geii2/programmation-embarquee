---
layout: layouts/page.njk
title: Travaux pratiques - 3
---

## Héritage

Créer une classe dérivée pour les personnes de moins de 25 ans


## Processus en arrière plan

Écrire un process qui va en arrière plan revaloriser un compte de type jeune

```csharp
public static void Process10s(object obj)
{
  CancellationToken ct = (CancellationToken)obj;
  do {
    Thread.Sleep(10_000);
    ToutRevaloriser();
  } while (!ct.IsCancellationRequested);
}
```
