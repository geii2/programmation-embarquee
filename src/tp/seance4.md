---
layout: layouts/page.njk
title: Travaux pratiques - 4
---

## Transactions

>Créer un enregistrement de type **transaction** lors de chaque opération.
{.objectif}

A. Proposer une structure de données. Une transaction est un enregistrement qui va stocker le type de l'opération, le montant de l'opération et la date et heure de l'opération.

```csharp
public class Transaction
{
  public string Type { get; private set; }
  public float Montant { get; private set; }
  public DateTime DateHeure { get; private set; }
}
```

Le constructeur de la classe qui permet d'affecter les paramètres lors d'un new aux champs de la classe.

```csharp
public Transaction(string Type, float Montant)
{
  this.Type = Type;
  this.Montant = Montant;
  this.DateHeure = DateTime.Now;
}
```

B. Définir la liste des transactions. Il y a une liste par compte.

Dans la classe compte. Définition et initiallisation de la liste des comptes.

```csharp
public List<Transaction> ListeDesTransactions { get; private set; } = new List<Transaction>();
```

C. Enregistrer les opérations (depot/retrait) dans la liste des transactions.

Ajouter (**Add**) une nouvelle (**new**) transaction dans la liste des transactions lors du dépôt.

```csharp
public void Depot(float montant)
{
  solde += montant;
  ListeDesTransactions.Add( new Transaction("depot", montant));
}
```

Ajouter (**Add**) une nouvelle (**new**) transaction dans la liste des transactions lors du retrait.

```csharp
 public void Retrait(float montant)
{
  solde -= montant;
  ListeDesTransactions.Add( new Transaction("retrait", montant));
}
```

D. Faire un test qui vérifie la cohérence entre le solde et l'historique des transactions.

opération|montant
---|--:
depot   |   100
retrait |  20
retrait |  10
depot   |   5
retrait |  50
**Total**  |   25

A vérifier avec le solde du compte

```csharp
public bool Verifier()
{
  float total = 0.0f;

  foreach (Transaction item in ListeDesTransactions)
  {
    if (item.Type == "depot") {
      total += item.Montant;
    }
    else if (item.Type == "retrait") {
      total -= item.Montant;
    }
  }

  if (total == solde)
  {
    return true;
  }
  else
  {
    return false;
  }
}
```

> Attention aux comparaisons entre nombres flottants. Les imprécisions rendent les comparaisons inexactes.
{.warning}

Il faut prendre la valeur absolue de la différence et comparer avec le plus petit nombre signaificatif acceptable

```csharp
return (Math.Abs(total - solde) < 0.01);
```

Le menu dans la classe MenuCompte

```csharp
[Menu("Vérifier les soldes", Position=4)]
public static void VerifierTout()
{
  foreach (Compte item in Program.ListeDesComptes)
  {
    Console.Write($" Compte {item.Numero} ({item.solde}) : ");
    Console.WriteLine(item.Verifier());
  }
}
```

## Communication avec un site web

>Appeler un service web qui va actualiser le cours d'une devise
{.objectif}

Nous allons utiliser un site internet qui propose un service de taux de changes de différents devises.
Ce site est [https://www.exchangerate-api.com/](https://www.exchangerate-api.com/)

L'adresse pour obtenir les informations de change est construite suivant ce modèle https://v6.exchangerate-api.com/v6/1a804e253ebe3f8007d60b68/pair/EUR/{devise}

```csharp
static async Task<double> DemandeChange(string devise)
{
  string adresse = $"https://v6.exchangerate-api.com/v6/1a804e253ebe3f8007d60b68/pair/EUR/{devise}";

  using (var webClient = new HttpClient())
  {
    string retour = await webClient.GetStringAsync(adresse);
    Console.WriteLine(retour);
  }
}
```

Le résultat est une grande chaine de texte, structurée avec des accolades `{` des propriétés suivi de `:`et de valeurs de type chaines de caractères (string), entier (int) et nombres réels (float/double).

```json
{"result":"success","documentation":"https://www.exchangerate-api.com/docs","terms_of_use":"https://www.exchangerate-api.com/terms","time_last_update_unix":1639526401,"time_last_update_utc":"Wed, 15 Dec 2021 00:00:01 +0000","time_next_update_unix":1639612801,"time_next_update_utc":"Thu, 16 Dec 2021 00:00:01 +0000","base_code":"EUR","target_code":"USD","conversion_rate":1.1281}
```
Cette structure de données appelée **JSON** est très utilisée sur Internet. Elle est utilisée nativement par le langage Javascript, le langage de tous les navigateurs web (Chrome, Firefox, ...)

Pour utiliser ces données il faut les convertir dans la structure de données que connait c# c'est à dire une **classe**

Utiliser le site [https://json2csharp.com](https://json2csharp.com) pour définir la classe à partir de la chaine de caractère représentant les données.

Le résultat est

```csharp
public class Exchangerate
{
  public string result { get; set; }
  public string documentation { get; set; }
  public string terms_of_use { get; set; }
  public long time_last_update_unix { get; set; }
  public string time_last_update_utc { get; set; }
  public long time_next_update_unix { get; set; }
  public string time_next_update_utc { get; set; }
  public string base_code { get; set; }
  public string target_code { get; set; }
  public double conversion_rate { get; set; }
}
```

Nous pouvons maintenant transformer la grande chaine de caractères en un objet de la classe Exchangerate. Cette opération de transformation s'appelle une **désérialisation**.

Nous utisons la fonction **Deserialize** de  la classe **JsonSerializer** (using System.Text.Json) qui traite les convertion au format JSON. Le résultat de la désérialisation est un objet de la classe **Exchangerate**  que l'on indique entre les caractères `< >`.

```csharp
ExchangeRate data = JsonSerializer.Deserialize<Exchangerate>(retour);
```

Une fois l'objet obtenu nous pouvons retrourner la propriété **conversion_rate**

```csharp
return data.conversion_rate;
```
La fonction complète :

```csharp
static async Task<double> DemandeChange(string devise)
{
  string adresse = $"https://v6.exchangerate-api.com/v6/1a804e253ebe3f8007d60b68/pair/EUR/{devise}";

  using (var webClient = new HttpClient())
  {
    string retour = await webClient.GetStringAsync(adresse);
    ExchangeRate data = JsonSerializer.Deserialize<Exchangerate>(retour);
    return data.conversion_rate;
  }
}
```
## Enregistrer

La sérialisation est le processus de conversion d’un objet en flux d’octets pour stocker l’objet ou le transmettre, une base de données ou un fichier. Son principal objectif est d’enregistrer l’état d’un objet afin de pouvoir le recréer si nécessaire. Le processus inverse est appelé désérialisation.

```csharp
 [Menu("Enregistrer", Position=4)]
public static void Enregistrer()
{
  string texte = JsonSerializer.Serialize(Program.ListeDesClients);
  File.WriteAllText("client.txt", texte);

  Console.ReadKey();
}
```
