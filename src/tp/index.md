---
layout: layouts/page.njk
title: Travaux pratiques
---

# Séance 1

## La solution

Une **solution** est un regroupement de projets, un ensemble de programmes.

Depuis le terminal Windows, créer une solution **Banque2021**

```shell-session
dotnet new sln -o Banque2021
```

Entrer dans le dossier de la solution.

```shell-session
cd Banque2021
```

## La librairie

Une libraire est un morceau de programme, un ensemble de classes qui offrent des fonctionnalités métiers. Une librairie est **réutilisable** dans  autant de programmes que l'on souhaite. Un projet **classlib** est une librairie composée de classes.

Créer une librairie qui porte le même nom que la solution : Banque2021

```shell-session
dotnet new classlib -o Banque2021
```

Ajouter la librairie `Banque2021` à la solution

```shell-session
dotnet sln add ./Banque2021/Banque2021.csproj
```

## L'interface console

Un programme **console** une un programme qui interagit avec l'utilisateur par le biais du clavier et de l'invite de commande. C'est une interface en mode texte, sans souris, sans graphique. C'est une interface très simple et qui utilise peu de ressources.

```shell-session
dotnet new console -o Automate
```

Ajouter le projet `Automate` à la solution

```shell-session
dotnet sln add ./Automate/Automate.csproj
```

Pour lancer le programme il est nécessaire de spécifier le projet, parmi tous ceux de la solution, qui va être exécuté.

```shell-session
dotnet run --project Automate
```
Ce programme contrôleur, cette interface, est une passerelle entre l'utilisateur et le code contenu dans les librairies.

Il convient d'ajouter une référence, un **lien**, de la librairie Banque2021 dans le programme console `Automate`.

```shell-session
dotnet add Automate/Automate.csproj reference Banque2021/Banque2021.csproj
```

## Le projet de tests

Un projet de test est un programme qui effectue automatiquement un ensemble de tâches. Chaque tâche vérifie une condition de fonctionnement du programme.

```shell-session
dotnet new xunit -o Banque2021.Tests
```

Ajouter le projet Banque2021.Tests à la solution

```shell-session
dotnet sln add ./Banque2021.Tests/Banque2021.Tests.csproj
```

Ajouter au projet de test la référence à la librairie car c'est présentement ce que l'on veut tester.

```shell-session
dotnet add Banque2021.Tests/Banque2021.Tests.csproj reference Banque2021/Banque2021.csproj
```

Lancer le programme de test

```shell-session
dotnet test
```

## La console

Se familiariser avec le fonctionnement de la [console](../console).

## Menu

Créer un Menu à l'aide de la classe `MenuController` fournie dans l'espace pédagogique.

Cette classe permet d'ajouter des attributs personnalisés sur les classes et méthodes du programme. Vos classes et méthodes sont eux-mêmes des objets avec des propriétés : nom, portée, liste des paramètres et attributs personnalisé.
L'introspection permet au programme de s'auto analyser et d'agir suivant ces propres méthodes.

Les **attributs** ajoutés sur les classes sont appelés des **décorations**.

1 Menu Principal
-  Client
-  Compte

2 Menu Client
- Lister
- Créer
- Supprimer
- Ouvrir compte

3 Menu Compte
- Lister
- Déposer
- Retirer

```csharp
[Menu("Banque 2021")]
class MenuPrincipal
{
  [Menu("Client", Position=1)]
  public static void Client()
  {
    MenuController.Show(typeof(MenuClient));
  }

  [Menu("Compte", Position=2)]
  public static void Compte()
  {
    MenuController.Show(typeof(MenuClient));
  }
}
```

Le menu Client

```csharp
[Menu("Client")]
class MenuPrincipal
{
  [Menu("Lister", Position=1)]
  public static void Lister()
  {
  }

  [Menu("Créer", Position=2)]
  public static void Creer()
  {
  }

  [Menu("Créer", Position=3)]
  public static void Supprimer()
  {
  }

  [Menu("Créer", Position=4)]
  public static void OuvrirCompte()
  {
  }
}
```

## La classe Client

Le client possède 2 propriétés son Nom et son Age. Le Nom est une chaîne de caractère `string`, l'Age une entier `ìnt`.

Il est bon de différencier l'accès en lecture (get) et en écriture (set). La lecture sera publique et l'écriture privée, réservée aux méthodes de la classe Client.

```csharp
using System;

namespace Banque2021;
{
  public class Client
  {
    public string Nom { get; private set; }

    public int Age { get; private set; }
  }
}
```
Le constructeur

```csharp
public Client(string Nom, int Age)
{
  this.Nom = Nom;
  this.Age = Age;
}
```


## La classe Compte

## Tests unitaires


Les **tests unitaires** permettent de vérifier le bon fonctionnement d’une petite partie bien précise (unité ou module) d’une application. Ils s'assurent qu'une méthode exposée à la manipulation par un utilisateur fonctionne bien de la façon dont elle a été conçue.

Effectuer des tests automatiques pour contrôler le dépôt et le retrait.

Premièrement se mettre dans les conditions de faire le test. Créer un client et créer un compte

Deuxièmement faire l'action à tester. Le dépôt d'un montant de 50 €

Troisièmement vérifier l'assertion qui est que le solde doit être égal à 50 €

On utilise la méthode Assert.Equal qui concerne un type float la valeur attendue est 50 et la valeur testée est compte1.solde

```csharp
[Fact]
public void TestDepot()
{
  Client client1 = new Client("Albert", 20.0f);
  Compte compte1 = new Compte(client1);

  compte1.Depot(50);
  Assert.Equal<float>(50.0f, compte1.solde);
}
```

Testons maintenant le retrait simple de 15 €

```csharp
[Fact]
public void TestRetrait()
{
  Client client1 = new Client("Albert", 20);
  Compte compte1 = new Compte(client1);

  compte1.Depot(50);
  compte1.Retrait(15);
  Assert.Equal<float>(35, compte1.solde);
}
```

Tester

```csharp
[Fact]
public void TestRetraitExcessif()
{
  Client client1 = new Client("albert", 20);
  Compte compte1 = new Compte(client1);

  compte1.Depot(50);

  Action retirer = () => compte1.Retrait(60);
  Assert.Throws<Exception>(retirer);

  Assert.Equal<float>(50, compte1.solde);
}
```

Tester la présence d'une levée d'exception (throw new Exception) lorsque l'age du client est supérieur à 100.

```csharp
public void TestClientAge()
{
  Action act = () => new Client("Albert", 150);
  Assert.Throws<Exception>(act);
}
```

Lancer les tests

```shell-session
dotnet test
```


## Séances

[séance 2](seance2)

[séance 3](seance3)

[séance 4](seance4)
