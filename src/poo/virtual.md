---
layout: layouts/page.njk
title: Machines virtuelles
---

Le Java, l'Objective-C, et le C# utilisent pour fonctionner des **machines virtuelles** (_runtime_). Ce sont des composants logiciels entre le programme et le processeur, ils s'occupent de gérer la mémoire à la place du programmeur. L'allocation de mémoire (malloc) et l'utilisation de pointeur n'est plus nécessaire. La mémoire est libérée (free) automatiquement par un composant de la machine virtuelle appelé **ramasse-miette** (_garbage collector_) qui scrute en permanence l'utilisation des objets.

De plus, la machine virtuelle utilise un langage précompilé ou **langage intermédiaire** qui n'est pas le langage machine natif. Les programmes sont donc portables pour peu que la machine virtuelle soit présente sur le système cible. La machine virtuelle traduit à la volée le langage intermédiaire en langage machine lors de l'exécution du code. La machine virtuelle prend aussi en charge la sécurité dans le cas des dépassements de capacité des variables, et la gestion des erreurs.

De fait, la machine virtuelle consomme de la mémoire et de la ressource processeur en surcharge du programme exécuté. Néanmoins cette surcharge est optimisée et très réduite ce qui dans les systèmes actuels, même mobiles, est complétement imperceptible.

Les bénéfices apportés à la programmation sont largement supérieurs à l'inconvénient d'utiliser une machine virtuelle.

L'utilisation de code classique avec gestion de la mémoire par pointeur et du processeur par le programmeur est toujours possible. Chez Microsoft l'utilisation de code non géré par la machine virtuelle et à la charge compète du développeur s'appelle du code non managé.

|	|Java	|.Net|	Objective C
|--|--|--|--|
Propriétaire|	Oracle|	Microsoft	|Apple
Machine virtuelle	|JVM (Java Virtual Machine)|	CLR (Common Language Runtime)	|Objective C runtime
Environnement de dév.	|JDK (Java Development Kit)	|.NET Framework (Windows) ou .Net Core (Linux/Mac)|	Cocoa
Langage de programmation|	Java, Kotlin	|C#, Vb .net, F#	|Objective C, Swift
Langage intermédiaire|	Bytecode	| MSIL (Microsoft Intermediate Language) |	-
Systèmes cibles|	Windows, Mac, Linux, Android, Téléphone (carte SIM), TV, Blu-ray disc |	Windows, Windows Phone ☹,  Windows 10 iot (Windows Embedded) ; Linux et Mac avec .Net Core |	Mac OS, iOS
IDE	|Netbeans, Eclispe, IntelliJ IDEA	|Visual Studio|	XCode


GC
: Garbage Collector ou ramasse-miettes est un sous-système informatique de gestion automatique de la mémoire. Il est responsable du recyclage de la mémoire préalablement allouée puis inutilisée.
