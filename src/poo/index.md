---
layout: layouts/page.njk
title: Programmation Orientée Objet
---

La **POO** reprend le désir de catégoriser le monde physique, de représenter les entités de manière atomique et de les ordonner. Un objet représente un concept, une idée ou toute entité du monde physique, comme une voiture, une personne ou encore une page d'un livre. Il possède des données internes et un comportement, et il sait interagir avec ses pairs.

Il s'agit donc de représenter ces objets et leurs relations ; l'interaction entre les objets via leurs relations permet de concevoir et réaliser les fonctionnalités attendues, de mieux résoudre le ou les problèmes. Dès lors, l'étape de modélisation revêt une importance majeure et nécessaire pour la POO. C'est elle qui permet de transcrire les éléments du réel sous forme virtuelle.

## Les langages de programmation orientée objet

### C++
Le C++ est un langage de programmation compilé dérivé du C. Ses bonnes performances, et sa compatibilité avec le C en font un des langages de programmation les plus utilisés dans les applications où la performance est critique.
Le C++ a été développé dans les années 1980. Il a été normalisé en 1998 par l'ISO et amendée ensuite en 2003. Une importante mise à jour a été ratifiée et publiée par l'ISO en septembre 2011 sous le nom de C++11. Depuis, des mises à jour sont publiées régulièrement afin de coller aux évolutions de langages concurrents : en 2014 C++14, en 2017 C++17. Une nouvelle version est prévue en 2020.

### Java
Java est un langage de programmation orienté objet créé en 1995 par Sun Microsystems. La société Sun a été ensuite rachetée en 2009 par la société Oracle qui détient et maintient désormais Java.

Le langage Java reprend en grande partie la syntaxe du langage C++. Néanmoins, Java a été épuré des concepts les plus complexes du C++, tels que les pointeurs, les références, ou l’héritage multiple. La particularité et l'objectif central de Java est que les logiciels écrits dans ce langage doivent être très facilement portables sur plusieurs systèmes d’exploitation tels que Unix, Windows, Mac OS ou Linux, avec peu ou pas de modifications, mais qui ont l'inconvénient d'être un peu plus lourd à l'exécution (en mémoire et en temps processeur) à cause de sa machine virtuelle.

### C#
Le C# (si charp) est un langage de programmation orientée objet, commercialisé par Microsoft depuis 2002. Il est dérivé du C++ et très proche du Java dont il se veut un concurrent. Il était exclusivement utilisé sur les machines Windows équipées de la plateforme .Net (dot net) pour réaliser des applications ou de sites web.
Depuis 2016 Microsoft publie la plateforme .Net Core, une version allégée de l'environnement .Net, qui est open source, gratuite et compatible Linux et Mac, les programmes écrit en C# peuvent s'exécuter maintenant sur ces plateformes.

### Objective C
Objective-C est un langage de programmation orienté objet réflexif. C'est une surcouche du langage C, comme le C++, mais qui se distingue de ce dernier par sa distribution dynamique des messages, son typage au choix faible ou fort, son typage dynamique et son chargement dynamique. Contrairement au C++, il ne permet pas l'héritage multiple Il était principalement utilisé dans les systèmes d'exploitation d'Apple : macOS et son dérivé iOS, avant qu'Apple ne développe la langage Swift.

## Différence entre le runtime et le SDK

Le **runtime** est la machine virtuelle qui héberge et exécute les applications. Il gère l'abstraction et la communication avec le système d'exploitation.

Le **SDK** comprend tous les outils pour générer et compiler une application.

Seul le runtime est nécessaire pour exécuter l'application. Le SDK est obligatoire pour pouvoir développer une application.

+ Installation du framework [.Net Core](../dotnet)
+ [Machines virtuelles](virtual)
+ [Classes](../classes)
+ [Méthode](../method)
+ [Données](../donnees)
+ [Enumeration](../enumeration)
+ [Constructeur](../constructeur)
+ [Héritage de classes](../heritage)

### Travaux pratiques

+ [Commandes utiles](commandes)
+ [Énoncé](tp)

POO
: Programmation Orientée Objet

SDK
: Software Development Kit
