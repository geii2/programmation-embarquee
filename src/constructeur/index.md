---
layout: layouts/page.njk
title: Constructeurs
---

Chaque fois qu’une classe est créée, une méthode d'initialisation appelée constructeur est exécutée. Les constructeurs permettent au programmeur de définir des valeurs par défaut, de limiter l’instanciation, d'initialiser les zones mémoires non managées.

Les constructeurs sont des fonctions qui portent le même nom que la classe. Ils initialisent généralement les membres de données du nouvel objet.

```csharp
public class Employé
{
   private int Salaire;

   public Employé (int salaireAnnuel)
   {
     Salaire = salaireAnnuel;
   }

   public Employé (int salaireHebdomadaire, int nombreDeSemaine)
   {
     Salaire = salaireHebdomadaire * nombreDeSemaine;
   }
}
```

Le constructeur sert ici à initialiser la donnée Salaire

```csharp
Employé e1 = new Employé(30000);
Employé e2 = new Employé(500, 52);
```

Une classe peut avoir plusieurs constructeurs qui prennent des arguments différents. Cette propriété s'appelle le polymorphisme.

## Finaliseurs / Destructeurs

Lorsqu'un objet cesse d'être utilisé il est détruit par l'utilisateur (en C++) ou dans le cas d'un code managé (Java, C#), par le ramasse miette. Une méthode, dite Finaliseur est appelée. Cette méthode sans paramètre est appelée automatiquement elle permet à l'objet de libérer la mémoire prise par les membres. Très utiles en C++, car le programmeur continue de gérer l'allocation mémoire des pointeurs, elle est exceptinnelle en Java et C#.

Contrairement au Constucteur il n'y a qu'une seule méthode Finaliseur.
La méthode Finaliseur (Finalizer) porte le même nom que la classe avec un ~ (tilde) devant. Elle n'a pas de paramètre.

```csharp
class Voiture
{
  ~Voiture()  // finalizer
  {
     // cleanup statements...
  }
}
```
