---
layout: layouts/page.njk
title: Méthodes
---

Les méthodes sont déclarées dans une classe en spécifiant :
- Le niveau d'accès comme public ou private, 
- Les modificateurs facultatifs comme abstract ou sealed, 
- La valeur de retour, 
- Le nom de la méthode et 
- Les éventuels paramètres de méthode. 

Ces parties forment ensemble la signature de la méthode.

Polymorphisme par surcharge / overloading : Une même méthode peut avoir des signatures différentes (essentiellement la liste des paramètres). Le compilateur choisi la bonne méthode à exécuter au vu du nombre et des types de paramètres passés.

Un type de retour d'une méthode ne fait pas partie de la signature de la méthode à des fins de surcharge de méthode. Toutefois, il fait partie de la signature de la méthode lors de la détermination de la compatibilité entre un délégué et la méthode vers laquelle il pointe. (Nous ne verrons pas ce concept dans ce cours)

Les paramètres de méthode sont placés entre parenthèses et séparés par des virgules. Des parenthèses vides indiquent que la méthode ne requiert aucun paramètre. 
