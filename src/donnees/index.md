---
layout: layouts/page.njk
title: Données
---

## Champs

Un champ est une variable de tout type qui est déclarée directement dans une classe. Les champs stockent les données qui sont accessibles à aux méthodes de classe et éventuellement, si le niveau d'accès le permet, à l'extérieur de la classe.


```csharp
class Etudiant 
{
  public string Nom;
  public int Age;
}
```

Pour accéder à un champ d'une variable de type objet, ajoutez un point après le nom de l’objet, suivi du nom du champ.

```csharp
Etudiant eleve = new Etudiant();
eleve.Nom = "Albert";
```

Un champ peut être de type valeur  :`int`, `bool`, `char` ou de type référence, c'est à dire une autre classe.

Il existe deux genres de types en C# : les **types valeur** qui contiennent directement leurs données et les **types référence** qui sont de fait des pointeurs vers les données (objets). 

Avec les types référence deux variables peuvent faire référence au même objet ; par conséquent, les opérations sur une variable peuvent affecter le même objet référencé par l'autre variable. 

Avec les types valeur, chaque variable a sa propre copie des données et les opérations sur une variable ne peuvent pas affecter l’autre.

Voir les [types valeurs](../types)

Les types référence sont les classes que vous créez, le seul type référence intégré à C# et utilisé fréquemment est la classe String qui représente les [chaînes de caractères](string). 


## Propriétés

Les propriétés sont des champs avec un mécanisme d'accès contrôlé. Les deux sont des membres nommés avec des types associés, et la syntaxe pour accéder aux champs et propriétés est la même.

Toutefois, contrairement aux champs, les propriétés ne désignent pas directement des emplacements de données. Au lieu de cela, les propriétés ont des méthodes appelées **accesseurs** qui spécifient les instructions à exécuter lorsque les valeurs sont lues ou écrites.

Les données sont masquées à l'utilisateur. Ce principe est appelé l'encapsulation.

### Accesseurs (getters) et mutateurs (setters)

```csharp
public string Nom { get; set; }
```

Ce code déclare une propriété Nom, qui correspond à un champ de données et deux méthodes pour accéder à cette donnée : une pour lire (get) appelée accesseur (getter) et l'autre pour écrire (set) appelée mutateur (setter).

Le champ de données est caché et réservé à une utilisation interne de la classe.

```csharp
private string _nom;

// Setter
public void Nom(string value){
	if (value.length > 2 && value.length < 30)
		_nom = value;
}

// Getter
public string Nom {
	return _nom;
}
```

La propriété s'utilise exactement de la même manière qu'un champ pour le code externe à la classe.

```csharp
eleve.Nom = "Martin";
```
Ce qui correspond en fait à un appel de la méthode set

```csharp
eleve.Nom("Martin")
```

Ce code appelle la méthode d'écriture (mutateur/setter) est stocke la valeur dans le champ privé _nom.
Tout ce mécanisme d'accès peut être écrit dans une syntaxe simplifiée et personnalisée pour offrir un contrôle complet des données. Par exemple en interdisant de mettre une propriété à vide

```csharp
public string Prenom
{
	get;
  set {
    if (string.IsNullOrWhiteSpace(value))
    throw new ArgumentException("La propriété Prenom ne peut être vide");
      firstName = value;
    }
  }
}
```

>Les membres champs d'une classe sont réservés à une utilisation privée de la classe, si le membre doit être accessible depuis l'extérieur de la classe alors il est fortement encouragé d'utiliser une propriété.
 
### Droit d'accès composite

L'intétêt des propriétés est de pouvoir différencier un accès différent à la lecture et à l'écriture.

```csharp
public string Prenom { get; private set; }
```
La lecture est publique, l'écriture est privée.

## Membres statiques

Une classe statique ne peut pas être instanciée. En d’autres termes, vous ne pouvez pas utiliser l’opérateur new pour créer une variable du type classe. Étant donné qu’il n’y a aucune variable d’instance, vous accédez aux membres d’une classe statique en utilisant le nom de classe lui-même.

```csharp
Math.Max(5, 8)
```

La classe Math est statique, les méthodes ne dépendent pas d'une instance d'objet mais ont un comportement général qui ne dépendent de rien d'autre que d'elles-mêmes.

Une classe instanciée, non statique, peut contenir des membres statiques. Le membre statique peut être appelé sur une classe même quand aucune instance de la classe n’a été créée. Le membre statique est toujours accessible par le nom de la classe, et non par le nom de l’instance.

Une seule copie d’un membre statique existe, quel que soit le nombre d’instances de la classe qui ont été créées. Les méthodes et propriétés statiques ne peuvent pas accéder à des champs ou des événements non statiques dans leur type contenant. Elles ne peuvent pas non plus accéder à une variable d’instance d’un objet.

Deux utilisations courantes des champs statiques consistent à conserver un compteur du nombre d’objets qui ont été instanciés ou à stocker une valeur qui doit être partagée entre toutes les instances.
