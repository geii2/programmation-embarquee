---
layout: layouts/page.njk
title: La classe
---

## Une super structure

Une structure est un **regroupement** de plusieurs champs de données de **types différents**. Ces données constituent une entité logique qui se réfère à un seul *objet*.

Exemple en C:
 
```c
typedef struct {
  char nom[20];
  char prenom[20];
  int anneeNaissance;
  float moyenne;
} etudiant;
```

La programmation orientée objet définit des structures constituées de données, comme `struct`, mais aussi des méthodes de comportement ou actions.  C'est l'équivalent d'une **super structure**.

```csharp
class Etudiant {

  string nom;
  string prenom;
  int anneeNaissance;
  float moyenne;

  int Evaluer();
  void Inscrire();
  void PorterAbsent(date jour);
  void Excuser();
}
```

## Déclaration

Les classes sont déclarés à l'aide du mot clé `class` suivit d'un identifiant unique qui donne le nom de la classe. Par convention cet identifiant est écrit avec une **Majuscule**.

Le mot clé class est précédé du **niveau d’accès**. Dans le cas suivant `public` est utilisé. Ce qui signifie que n’importe qui peut utiliser cette classe.

Le reste de la déclaration est le **corps** de la classe, dans lequel les **données** et les **actions** sont définis.

## Membres

> ***Définition :*** Les champs, propriétés, méthodes et événements d’une classe sont désignés collectivement par le terme **« membres de classe »**.
{.definition}

```csharp
public class Etudiant
{
  Membres de la classe étudiant =
  Données :
    - Champs
    - Propriétés

  Actions :
    - Méthodes
    - Événements
}
```

## Type de données

Tout comme la structure l'objet est un nouveau **type de données** lequel peut être utilisé pour la **déclaration de variable**. Ce nouveau type est appelé une classe (`class`).

Des **variables** peuvent être associées à ce type.

```csharp
int a;
Etudiant p;
Etudiant etudiant;
```

Dans cet exemple on déclare une variable `a` de type `int` (entier), une variable `p` de type `Etudiant`, une variable `etudiant` de type `Etudiant`. Attention la casse est importante.

Un type défini comme `class` est un type **référence**. Les variables de type `class` sont donc des **pointeurs**.


## Instance

Au moment de l’exécution, quand vous déclarez une variable de type référence (classe), celle-ci contient la valeur `Null` tant que vous n’avez pas explicitement créé une **instance** de la classe à l’aide de l’opérateur `new`, ou que vous ne lui avez pas assigné un objet existant d’un type compatible.

Vous créez des objets en utilisant le mot clé `new` suivi du nom de la classe sur laquelle l’objet est basé.

Déclaration d'une variable `eleve` de type Etudiant.

```csharp
Etudiant eleve;
```

Création de l'instance pour la variable `eleve`. C'est à dire réservation de la mémoire pour ses champs de données.

```csharp
eleve = new Etudiant();
```
Déclaration et création sur une même ligne de code

```csharp
Etudiant eleve = new Etudiant();
```

Étant donné que le mot clé Etudiant est cité 2 fois dans une même ligne il possible d'utiliser une déclaration implicite avec le mot clé `var`

```csharp
var eleve = new Etudiant();
```

eleve est forcement une variable de type Etudiant.

## Définition

A connaître par coeur

Bien qu’ils soient parfois employés indifféremment, une classe et un objet sont deux choses différentes. Une classe définit un **type** d’objet, mais il ne s’agit pas d’un objet en soi. Un objet, qui est une entité concrète basée sur une classe, est parfois désigné par le terme **« instance de classe »**.

>Etudiant est un **type**\
Etudiant est un **type référence** ( = type pointeur )\
Etudiant est une **classe**
{.info}

>eleve est un **objet** de type Etudiant\
eleve est une **instance de la classe** Etudiant. (Une fois new effectué)\
eleve est un **pointeur** vers une zone mémoire contenant les données de la classe Etudiant
{.info}

## Utilisation

### Référence null

```csharp
Etudiant eleve2;
```

Il est déconseillé de créer des références d’objet, sans créer une instance d'objet avec l'opérateur `new`, car toute tentative d’accès à et objet génère une erreur au moment de l’exécution.

>La référence d'objet n'est pas définie à une instance d'un objet.
{.danger}

Ce qui veut dire que le pointeur (référence) ne pointe pas vers une zone mémoire allouée pour une instance de la classe.

Vous pouvez toutefois créer une telle variable et faire référence à un objet existant.

```csharp
Etudiant eleve1 = new Etudiant();
Etudiant eleve2 = eleve1;
```

Ce code crée deux objets (pointeurs) qui font tous deux références à la même instance d'objet (zone mémoire contenant les données). Toute modification apportée à l’objet par le biais de eleve2 est donc reflétée dans eleve1.

> Un type défini comme **class** est un type référence.
{.definition}

Source : [programming guide](https://docs.microsoft.com/fr-fr/dotnet/csharp/programming-guide/classes-and-structs/classes)
