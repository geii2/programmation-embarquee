---
layout: layouts/page.njk
title: Console
---

> La console est un système informatique constituée d'une sortie au format **texte** et d'une entrée au **clavier** uniquement.
{.definition}


## Accès à la console 

La console peut être directe sur la machine  sur laquelle on travaille ou déportée, sur une machine distante.

Sur Windows le programme `invite de commande` permet de se connecter à la console de l'ordinateur. Sur les dernières mises à jour de Windows, notamment Windows 11, le programme [`Windows Terminal`](https://www.microsoft.com/fr-fr/p/windows-terminal/9n0dx20hk701?activetab=pivot:overviewtab) permet de bénéficier d'améliorations confortables (copier/coller, couleurs, accents et caractères spéciaux)

La console est géré par la classe `Console`. Comme il n'y qu'une seule console par machine les membres sont statiques il n'est pas nécessaire d'utiliser la commande `new` pour instancier la console. Toutes les commandes s'utilisent directement sur la classe `Console`.

## Commandes de la console

Effacer l'écran

```csharp
Console.Clear();
```

Écrire du texte à l'écran

```csharp
Console.Write("Bonjour");
```

Écrire du texte et retourner à la ligne

```csharp
Console.WriteLine("Au revoir");
```

qui est équivalent à

```csharp
Console.Write("Au revoir\n");
```

### Boites

Des caractères spéciaux sont utiliser pour dessiner des boîtes

```
┌─────┬─────┐   ╔════╦═════╗
│     │     │   ║    ║     ║
├─────┼──╥──┤   ╠════╬══╤══╣
╞═════╪══╬══╡   ╟────╫──┼──╢
└─────┴──╨──┘   ╚════╩══╧══╝

╒═════════╕   ╓──────────╖
│         │   ║          ║
┝━━━━━━━━━┥   ╟┄┄┄┄┄┄┄┄┄┄╢
│         │   ╟╌╌╌╌╌╌╌╌╌╌╢
│         │   ╟┄┄┄┄┄┄┄┄┄┄╢
╘═════════╛   ╙──────────╜

┏━━━━┳━━━━┓
┃    ┃    ┃
┣━━━━╋━━━━┫
┃    ┃    ┃
┗━━━━┻━━━━┛
```


### Les couleurs

La couleur d'écriture

```csharp
Console.ForegroundColor = ConsoleColor.Red;
```

La couleur de fond

```csharp
Console.BackgroundColor = ConsoleColor.Yellow;
```

Retour aux couleurs par défaut

```csharp
Console.ResetColor();
```

Dans Windows Terminal il est possible d'écrire une commande pour changer la couleur du texte en cours d'écriture. Ces commandes ne fonctionnent pas dans l'invite de commande. Les commandes sont déclencher par le caractère d'échappement Esc qui s'écrit `\x1b` en C#.

Couleur|Texte|Fond|Texte clair|Fond clair
---|---|---|---|---
Noir    | Esc[30m |	Esc[40m |	Esc[90m |	Esc[100m
Rouge   | Esc[31m |	Esc[41m |	Esc[91m |	Esc[101m
Vert    | Esc[32m |	Esc[42m |	Esc[92m |	Esc[102m
Jaune   | Esc[33m |	Esc[43m |	Esc[93m |	Esc[103m
Bleu 	  | Esc[34m |	Esc[44m |	Esc[94m |	Esc[104m
Magenta | Esc[35m |	Esc[45m | Esc[95m |	Esc[105m
Cyan  	| Esc[36m |	Esc[46m | Esc[96m | Esc[106m
Blanc   | Esc[37m | Esc[47m | Esc[97m | Esc[107m
 	
Fonction|Code
---|---
Plus clair ou gras | Esc[1m 
Souligné | Esc[4m	 
Non souligné | Esc[24m
Inversion des couleurs | Esc[7m  	 
Non inversion des couleurs | Esc[27m  	  	 
Par défaut | Esc[0m

```csharp
Console.Write("\x1b[91mBonjour\x1b[0m tout le \x1b[4mmonde\x1b[24m");
```

### Les entrées au clavier

Lire un texte au clavier. La validation se fait avec la touche `Entrée`

```csharp
string nom;
nom = Console.ReadLine();
```

Lire une touche au clavier. Cette lecture est unique.

```csharp
ConsoleKeyInfo c;
c = Console.ReadKey(true);
```

Cette fonction retourne un objet de type `ConsoleKeyInfo`. 

Cet objet contient une propriété `Key` qui correspond au code de la touche pressée. Ce code est un numérique, mais pour pouvoir utiliser ces codes facilement une énumération permet de leur attribuer des étiquettes.

Par exemple : `ConsoleKey.D5` pour la touche du chiffre 5 (Digit 5)

Le propriété `Modifiers` permet de savoir quelle deuxième touche de modification est pressée en même temps que la touche active. Les touches de modification sont Ctrl, Shift et Alt.

Le paramètre `true` de la fonction permet de capturer la touche avant qu'elle ne s'affiche à l'écran.

Le code suivant est une boucle qui lit une frappe au clavier et qui s'arrête lorsque l'on appuie sur la touche Q et sur la touche de modification Ctrl

```csharp
ConsoleKeyInfo c;
do {
  c = Console.ReadKey(true);
          
} while(c.Key!=ConsoleKey.Q || c.Modifiers != ConsoleModifiers.Control);
```